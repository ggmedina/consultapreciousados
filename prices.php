<?php
function get_http_response_code($url) {
    $headers = get_headers($url);
    return substr($headers[0], 9, 3);
}
$brandId = $_REQUEST["brand"];
$modelId = $_REQUEST["model"];
$versionId = $_REQUEST["version"];
if($versionId != 0){
    $urlPrices = 'http://autocity_api.test//prices/brands/'.$brandId.'/models/'.$modelId.'/versions/'.$versionId;
    
    if(get_http_response_code($urlPrices) != "200"){
        echo "";
    }
    else
    {
        $arrayPrices = json_decode( file_get_contents($urlPrices), true );
        $countPrices = count($arrayPrices);
        if( $countPrices > 0 ){
           $arrayPrices = $arrayPrices['data'];
           $countPrices = count($arrayPrices);
           if( $countPrices > 0 ){   
                echo "<div style='float: left;width: 30%;margin-left: 10%;margin-top: 10px;margin-bottom: 10px;'><table style='cursor:pointer;'>";
                echo "<tr>";
                echo "   <td style='border: 1px solid gray;padding: 8px;background-color: #e0e0e0;text-align: center;color: gray; font-weight: bold;'>Modelo</td>";
                echo "   <td style='border: 1px solid gray;padding: 8px;background-color: #e0e0e0;text-align: center;color: gray; font-weight: bold;'>Precio</td>";
                echo "</tr>";
                $arrayPricesUnique = '';
                $arrayPricesUnique = $arrayPrices['prices'];
                foreach ($arrayPricesUnique as $valorPrices){
                    echo "<tr>";
                    //if( $valorPrices['year'] == '2018' )
                    //{
                    //    echo "   <td style='border: 1px solid gray;padding: 8px;text-align: center;color: #000000; '>" . "0Km" . "</td>";
                    //}
                    //else
                    //{
                        echo "   <td style='border: 1px solid gray;padding: 8px;text-align: center;color: #000000; '>" . $valorPrices['year'] . "</td>";
                    //}
                    
                    //$tablePrices .= "   <td>" . $valorPrices['amount'] . "</td>";
                    $newPrices = $valorPrices['amount'] . "000";
                    echo "   <td style='border: 1px solid gray;padding: 8px;text-align: center;color: #000000; '>" . "$  " . number_format($newPrices) . "</td>";
                    echo "</tr>";
                }
                echo "</table></div>";
                
                echo "<div id='div_accessories' style='float: left;width: 40%;margin-top: 10px;margin-bottom: 10px;'>";
                //-------------------------------------------------------------------
                $arrayAccessories = $arrayPrices['accessories'];
                $arrayAccessories2 = $arrayPrices['accessories2'];
                $arrayAccessories3 = $arrayPrices['accessories3'];
                //-------------------------------------------------------------------
                $secMotorYTransmision = "<h3>Motor y Transmision</h3>";
                $secMotorYTransmision .= "<div>";
                $secMotorYTransmision .= "  <ul>";
                //-------------------------------------------------------------------
                $secConfort = "<h3>Confort</h3>";
                $secConfort .= "<div>";
                $secConfort .= "  <ul>";
                //-------------------------------------------------------------------
                $secSeguridad = "<h3>Seguridad</h3>";
                $secSeguridad .= "<div>";
                $secSeguridad .= "  <ul>";
                //-------------------------------------------------------------------
                $secDatosTecnicos = "<h3>Datos Tecnicos</h3>";
                $secDatosTecnicos .= "<div>";
                $secDatosTecnicos .= "  <ul>";
                //-------------------------------------------------------------------
                foreach ($arrayAccessories as $accessorie){
                    $secMotorYTransmision .= "<li>Combustible: " . $accessorie['ext_combu'] . " </li>";
                    $secMotorYTransmision .= "<li>Alimentacion: " . $accessorie['ext_alime'] . " </li>";
                    $secMotorYTransmision .= "<li>Cilindrada: " . $accessorie['ext_motor'] . " </li>";
                    $secMotorYTransmision .= "<li>Velocidad max: " . $accessorie['ext_veloc'] . " </li>";
                    $secMotorYTransmision .= "<li>Potencia: " . $accessorie['ext_poten'] . " </li>";
                    $secMotorYTransmision .= "<li>Traccion: " . $accessorie['ext_tracc'] . " </li>";
                    $secMotorYTransmision .= "<li>Transmision: " . $accessorie['ext_cajav'] . " </li>";
                    //--
                    $secConfort .= "<li>Aire Acondicionado: " . $accessorie['ext_airea'] . " </li>";
                    //--
                    $secSeguridad .= "<li>Frenos Abs: " . $accessorie['ext_frabs'] . " </li>";
                    $secSeguridad .= "<li>Airbag: " . $accessorie['ext_airba'] . " </li>";
                    //--
                    $secDatosTecnicos .= "<li>Puertas: " . $accessorie['ext_puert'] . " </li>";
                    $secDatosTecnicos .= "<li>Clasificación: " . $accessorie['ext_clasi'] . " </li>";
                    $secDatosTecnicos .= "<li>Cabina (pick ups): " . $accessorie['ext_cabin'] . " </li>";
                    $secDatosTecnicos .= "<li>Permite carga: " . $accessorie['ext_carga'] . " </li>";
                    $secDatosTecnicos .= "<li>Peso: " . $accessorie['ext_pesot'] . " </li>";
                    $secDatosTecnicos .= "<li>Dirección: " . $accessorie['ext_direc'] . " </li>";
                }
                foreach ($arrayAccessories2 as $accessorie2){
                    $secConfort .= "<li>Climatizador: " . $accessorie2['ex2_clima'] . " </li>";
                    $secConfort .= "<li>Techo corredizo: " . $accessorie2['ex2_tcorr'] . " </li>";
                    $secConfort .= "<li>Antiniebla: " . $accessorie2['ex2_fanti'] . " </li>";
                    $secConfort .= "<li>Sensor de estacionamiento: " . $accessorie2['ex2_sesta'] . " </li>";
                    //--
                    $secSeguridad .= "<li>Airbag lateral: " . $accessorie2['ex2_alate'] . " </li>";
                    $secSeguridad .= "<li>Airbag cabeza conductor y acompañante: " . $accessorie2['ex2_acabe'] . " </li>";
                    $secSeguridad .= "<li>Airbag cortina: " . $accessorie2['ex2_acort'] . " </li>";
                    $secSeguridad .= "<li>Airbag rodilla: " . $accessorie2['ex2_arodi'] . " </li>";
                    $secSeguridad .= "<li>Fijación ISOFIX: " . $accessorie2['ex2_isofi'] . " </li>";
                    $secSeguridad .= "<li>Control de tracción: " . $accessorie2['ex2_ctrac'] . " </li>";
                    $secSeguridad .= "<li>Control de estabilidad: " . $accessorie2['ex2_cesta'] . " </li>";
                    $secSeguridad .= "<li>Control de descenso: " . $accessorie2['ex2_cdesc'] . " </li>";
                    $secSeguridad .= "<li>Control dinámico de conducción: " . $accessorie2['ex2_cdina'] . " </li>";
                    $secSeguridad .= "<li>Sistema de arranque en pendiente: " . $accessorie2['ex2_sapen'] . " </li>";
                    $secSeguridad .= "<li>Bloqueo diferencial: " . $accessorie2['ex2_bdife'] . " </li>";
                    $secSeguridad .= "<li>Repartidor electrónico de frenado: " . $accessorie2['ex2_relef'] . " </li>";
                    $secSeguridad .= "<li>Asistente frenado de emergencia: " . $accessorie2['ex2_afree'] . " </li>";
                    $secSeguridad .= "<li>Regulador de par de frenado: " . $accessorie2['ex2_rparf'] . " </li>";
                    //--
                    $secDatosTecnicos .= "<li>Largo: " . $accessorie2['ex2_largo'] . " </li>";
                    $secDatosTecnicos .= "<li>Alto: " . $accessorie2['ex2_alto'] . " </li>";
                    $secDatosTecnicos .= "<li>Ancho: " . $accessorie2['ex2_ancho'] . " </li>";
                }
                foreach ($arrayAccessories3 as $accessorie3){
                    $secConfort .= "<li>Tapizado de cuero: " . $accessorie3['ex3_tapcu'] . " </li>";
                    $secConfort .= "<li>Asientos electrónicos: " . $accessorie3['ex3_aelec'] . " </li>";
                    $secConfort .= "<li>Computadora de abordo: " . $accessorie3['ex3_cabor'] . " </li>";
                    $secConfort .= "<li>Faros de xenon: " . $accessorie3['ex3_fxeno'] . " </li>";
                    $secConfort .= "<li>Llantas de aleación: " . $accessorie3['ex3_lalea'] . " </li>";
                    $secConfort .= "<li>Techo panorámico: " . $accessorie3['ex3_tpano'] . " </li>";
                    $secConfort .= "<li>Volante con levas: " . $accessorie3['ex3_vleva'] . " </li>";
                    $secConfort .= "<li>Bluetooth: " . $accessorie3['ex3_bluet'] . " </li>";
                    $secConfort .= "<li>Asientos térmicos: " . $accessorie3['ex3_aterm'] . " </li>";
                    //--
                    $secSeguridad .= "<li>Sensor de lluvia: " . $accessorie3['ex3_slluv'] . " </li>";
                    $secSeguridad .= "<li>Sensor crepuscular: " . $accessorie3['ex3_screp'] . " </li>";
                    $secSeguridad .= "<li>Indicador de presión de neumáticos: " . $accessorie3['ex3_ipneu'] . " </li>";
                    $secSeguridad .= "<li>Run flat: " . $accessorie3['ex3_rflat'] . " </li>";
                }
                //-------------------------------------------------------------------
                $secMotorYTransmision .= "  </ul>";
                $secMotorYTransmision .= "</div>";
                //-------------------------------------------------------------------
                $secConfort .= "  </ul>";
                $secConfort .= "</div>";
                //-------------------------------------------------------------------
                $secSeguridad .= "  </ul>";
                $secSeguridad .= "</div>";
                //-------------------------------------------------------------------
                $secDatosTecnicos .= "  </ul>";
                $secDatosTecnicos .= "</div>";
                //-------------------------------------------------------------------
                echo $secMotorYTransmision;
                echo $secConfort;
                echo $secSeguridad;
                echo $secDatosTecnicos;
                echo "</div>";
           }
        }
        else{
            echo "";
        }
    }
}
else {
    echo "";
}
?>