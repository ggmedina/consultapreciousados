<html>
    <header>
        <link rel=StyleSheet href="css/styles.css" type="text/css" />
        <link rel=StyleSheet href="css/select2.min.css" type="text/css" />
        
        <!--<nav class="navbar navbar-default navbar-fixed-top"> -->
        <div style="background-color: #000000;float: left;width: 100%;height: 200%;">    
            <div style="position: absolute;top: 1%;left: 5%;">
                <img src="img/autocity-logo.png" style="height: 80px;"/>
            </div>
            <div style="position: absolute;top: 20px;left: 32%; color: #FFFFFF; font-weight: bold; font-family: Raleway; font-size: 40;">
                <span>CONSULTA DE PRECIOS</span>
            </div>
            <div style="position: absolute;top: 1%;right: 5%;">
                <!--<img src="img/infoauto_logo.png" style="height: 12%;"/>-->
                <!--<img src="img/infoauto_logo.png" style="height: 80px;"/>-->
            </div>
        </div>
        <!--</nav>--> 
        
        <script language="javascript" src="js/jquery-3.2.1.min.js"></script>
         <script src="js/select2.min.js"></script>
        
         
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <style>
            #div_accessories h3,ul{ text-align:left; }
            #div_accessories .ui-accordion-header.ui-state-active { background: #0fa4b8; }
            #div_accessories{ font-family: Raleway; }
            #div_accessories li {
                cursor: pointer;
            }
            #div_accessories li:hover {
                font-weight:bold;
            }
        </style> 
         
    </header>
<body style="font-family: Raleway;line-height: 1.4;font-size: 14px;color: gray;background-color: #f0f0f0;">
    <div style="height: 4%;float: left;width: 100%;">
    </div>
    <div style="margin-left: 10%;text-align: center;background-color: #0fa4b8;padding: 20px;width: 80%;float: left;border-top-left-radius: 10px;border-top-right-radius: 10px;">
        <div style="width: 100%;float: left;">
            <div style="float: left;width: 45%;text-align: right;">
                <span style="color: #fff;margin-right: 2px;">Marca: </span>
            </div>
            <div style="width: 55%;float: right;text-align: left;">
                <!--<select id="slcBrands" clas="slcFilter" style="padding: 4px;cursor:pointer; margin-left: 2px;">-->
                <select id="slcBrands">
                    <option value="0" >--Seleccione--</option>
                </select>
            </div>
        </div>
        <div style="width: 100%;float: left;">
            <div style="float: left;width: 45%;text-align: right;">
                <span style="color: #fff;margin-right: 2px;">Modelo: </span>
            </div>
            <div style="width: 55%;float: right;text-align: left;">
                <!--<select id="slcModels" clas="slcFilter" style="padding: 4px;cursor:pointer; margin-left: 2px;">-->
                <select id="slcModels">
                    <option value="0" >--Seleccione--</option>
                </select>
            </div>
        </div>
        <div style="width: 100%;float: left;">
            <div style="float: left;width: 45%;text-align: right;">
                <span style="color: #fff;margin-right: 2px;">Version: </span>
            </div>
            <div style="width: 55%;float: right;text-align: left;">
                <!--<select id="slcVersions" clas="slcFilter" style="padding: 4px;cursor:pointer; margin-left: 2px;">-->
                <select id="slcVersions" >
                    <option value="0" >--Seleccione--</option>
                </select>
            </div>
        </div>
        
    </div>
    <div style="margin-left: 10%;text-align: center;background-color: #ffffff;width: 80%;float: left;">
        <div align="center" id="divPrices" style="display:none;">
        </div>
        <div id="divLoading" style="display:none;padding: 20px;">
            <span class="parpadea" >C A R G A N D O   .   .   .</span>
        </div>
    </div>
    
 </body>
    
   
    <script language="javascript">
    $(document).ready(function(){
        
        $('#slcBrands').select2();
        $('#slcModels').select2();
        $('#slcVersions').select2();
        
        $('#slcBrands').select2({dropdownAutoWidth: 'true'});
        $('#slcModels').select2({dropdownAutoWidth: 'true'});
        $('#slcVersions').select2({dropdownAutoWidth: 'true'});
        
        $("#divLoading").show();
        $.ajax({url: "brands.php", success: function(result){
            $("#divLoading").hide();
            $("#slcBrands").html(result);
        }});
        
        /*
        $.post("brands.php", "", function(data){
                $("#divLoading").hide();
                $("#slcBrands").html(data);
        });
        */
       $("#slcBrands").change(function () {
           $("#slcModels").html('<option value="0" >--Seleccione--</option>');
           $("#slcVersions").html('<option value="0" >--Seleccione--</option>');
           $("#divPrices" ).html('');
           //$("#divPrices" ).removeAttr( "style" ).fadeOut();
           $("#divPrices" ).hide().fadeOut();
           $("#divLoading").show();
                /*
                $("#slcBrands option:selected").each(function () {
                 brand=$(this).val();
                 $.post("models.php", { brand: brand }, function(data){
                    $("#divLoading").hide();
                    $("#slcModels").html(data);
                 });*/
                 $("#slcBrands option:selected").each(function () {
                 brand = $(this).val();
                 $.ajax({url:"models.php",  data:{ brand: brand } , success:function(result){
                    $("#divLoading").hide();
                    $("#slcModels").html(result);
                 }});
             });
        });
       $("#slcModels").change(function (){
            $("#slcVersions").html('<option value="0" >--Seleccione--</option>');
            $("#divPrices" ).html('');
            //$("#divPrices" ).removeAttr( "style" ).fadeOut();
            $("#divPrices" ).hide().fadeOut();
            $("#divLoading").show();
            $("#slcModels option:selected").each(function () {
                brand = $("#slcBrands option:selected").val(); 
                model = $(this).val();
                 /*$.post("versions.php", { brand: brand , model: model }, function(data){
                    $("#divLoading").hide();
                    $("#slcVersions").html(data);
                 });*/
                $.ajax({url:"versions.php", data:{ brand: brand , model: model }, success:function(result){
                    $("#divLoading").hide();
                    $("#slcVersions").html(result);
                 }});
            });
        });
        $("#slcVersions").change(function () {
            $("#divPrices" ).html('');
            $("#slcVersions option:selected").each(function () {
                model = $("#slcModels option:selected").val();
                brand = $("#slcBrands option:selected").val(); 
                version=$(this).val();
                $("#divLoading").show();
                /*$.post("prices.php", { brand: brand , model: model , version: version}, function(data,status){
                    //alert('status: '+status);
                    $("#divLoading").hide();
                    $("#divPrices").html(data);
                    $("#divPrices" ).show( "blind" );
                });*/            
                $.ajax({url:"prices.php", data:{ brand: brand , model: model , version: version}, success:function(result){
                    $("#divLoading").hide();
                    $("#divPrices").html(result);
                    $("#divPrices" ).show( "blind" );
                    
                    $( "#div_accessories" ).accordion({
                        collapsible: true,
                        heightStyle: "content",
                        active: false,
                        icons: { "header": "ui-icon-plus #E74C3C", "activeHeader": "ui-icon-minus" }
                      });
                }});
            });
        });
        
        
    });
    </script>
    
</html>