<?php
$brandId = $_REQUEST["brand"];
$modelId = $_REQUEST["model"];
$urlVersions = 'http://autocity_api.test/prices/brands/'.$brandId.'/models/'.$modelId.'/versions';
$arrayVersions = json_decode( file_get_contents($urlVersions), true );
if( count($arrayVersions) > 0 ){
    $arrayVersions = $arrayVersions['data'];
    if( count($arrayVersions) > 0 ){
        echo "<option value='0' >" . "--Seleccione--" . "</option>";
        foreach ($arrayVersions as $valor){
            echo "<option value='" . $valor['id'] . "' >" . $valor['name'] . "</option>";   
        }
    }
}
?>